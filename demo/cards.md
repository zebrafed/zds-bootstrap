#Zebra Cards Documentation 

###How to use?
Full reference can be found at (https://getbootstrap.com/docs/4.0/components/card/)

##Making a card
- Apply the .card class 
- Color: The default background color is off-white. It can be changed to blue using .bg-primary or changed to white with a grey border using .card-outline.
- Text: Any text should be put in a .card-body class.
    - Titles should have the .card-title class applied. By default, the text is blue.
    - Subtitles and overlines should use the .card-subtitle class. Overlines should also use the .overline class to achieve the correct spacing.
    - Paragraph text should use the .card-text class.
    - By default, subtitle text and card text is black.
    - All text in cards with .bg-primary applied will be white by default.
    - Margins can be given to the top of paragraph text using the .p-mt-1, .pt-mt-2, .p-mt-3 classes
- Anything below the card body should be contained in a .card-footer class.

##Examples
An off-white card with a title and subtitle:
```javascript
<div class="card">
    <div class="card-body">
        <h1 class="card-title">Title goes here</h1>
        <h2 class="card-subtitle">Subtitle goes here</h2>
    </div>
</div>
```
An off-white card with body text and a link icon:
```javascript
<div class="card">
    <div class="card-body">
        <h2 class="subtitle">Card information that can be long and descriptive. Here we can talk about features of a product or a solution that we offer.</h2>
    </div>
    <div class="card-footer">
        <img src="img/open_in_new_window_grey.svg" class="square-image">
    </div>
</div>
```
An off-white card with a title, text and a button:
```javascript
<div class="card">
    <div class="card-body">
        <h1 class="card-title" style="color: black">Title goes here</h1>
        <p class="card-text p-mt-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
        ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim v eniam, quis nostrud exerci tation
        ullamcorper suscipit lobortis.</p>
    </div>
    <div class="card-footer justify-content-end d-flex">
        <button class="btn btn-primary">Standard button</button>
    </div>
</div>
```
A white, bordered card with a title and subtitle:
```javascript
<div class="card card-outline">
    <div class="card-body">
        <h1 class="card-title text-blue">Title goes here</h1>
        <p class="subtitle">Subtitle goes here</p>
    </div>
</div>
```
A blue card with an overline, title, text and a button:
```javascript
<div class="card bg-primary">
    <div class="card-body">
        <h2 class="subtitle overline text-white">Overline</h1>
        <h1 class="card-title text-white">Title goes here</h2>
        <p class="card-text p-mt-2 text-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy nibh euismod tincidunt
            ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
            ullamcorper suscipit lobortis.</p>
    </div>
    <div class="card-footer justify-content-end d-flex">
        <button class="btn btn-light">Standard button</button>
    </div>
</div>
```
##Image cards
- Used for displaying images with a caption.
- Apply the .image-card class to the container which is going to contain the image.
- Put the image in the container and apply the .card-img-top class.
- Apply the .card-footer class to the container which is going to contain the caption and apply .card-text to the text.

Here's an example image card with a caption and a link icon:
```javascript
<div class="image-card md-2 mb-2">
    <img class="card-img-top" src="img/4x3_placeholder.JPG">
    <div class="card-footer d-flex flex-row align-items-center">
        <div class="card-text mr-auto">H5 Headline</div>
        <img src="img/open_in_new_window_white.svg" class="square-image">
    </div>
</div>
```