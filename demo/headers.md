###How to use?
For a full reference go to (https://getbootstrap.com/docs/4.0/components/navbar/)

##Header types
- Navbar: .navbar is the class used for the headers.
- Navbar secondary: .navbar-secondary is used as an additional part of the header containing items below the main header.
- Navbar narrow: .navbar-narrow is a sub-container for a header and it's used when a large margin is needed on items inside the header.
- Navbar small: .navbar-small is designed for smaller window sizes. The main differences are the size of the Zebra logo in the navbar brand and the drop-down items in .navbar-small containers are smaller. 
- *Note: Navbar classes should always be applied to '<header>' tags.*

##Components
- Nav item: .nav-item classes are used for navigation elements on the headers. By default, the navigation elements are tabs. Nav items should always be contained in a .nav-tabs container
    - Drop-down: The .drop-down class can be applied to a nav item to turn it into a drop-down element with an icon.
    - Active: Applying .active to a tab makes the text black and gives the tab a blue bottom border. Applying .active to a drop-down item also turns the text black and turns the drop-down icon blue. 
- Navbar brand: .navbar-brand is found on all the headers and contains the Zebra logo.
- Search boxes: .search-container is used to contain a search box and a search icon.
    - .search-container-bottom is used for large search bars underneath a header.

##Examples
An empty header with a Zebra logo:
```javascript
<div class="navbar">
    <a class="navbar-brand"></a>
</div>
```
An empty, narrow header with a Zebra logo:
```javascript
<div class="navbar">
    <div class="navbar-narrow m-auto w-100">
        <a class="navbar-brand"></a>
    </div>
</div>
```
A header with a search box:
```javascript
<div class="navbar">
    <a class="navbar-brand"></a>
    <div class="search-container d-flex flex-nowrap">
        <div class="search-box">Search box</div>
        <div class="search-icon"></div>
    </div>
</div>
```
A narrow header with a search bar:
```javascript
<div class="navbar d-flex flex-wrap">
    <div class="navbar-narrow m-auto align-items-center">
        <a class="navbar-brand"></a>
    </div>
</div>
<div class="search-container-bottom d-flex align-items-center">
    <div class="navbar-narrow m-auto d-flex align-items-center" style="max-width: 1280px; width: 100%;"> 
        <div class="search-icon"></div>
        <div class="search-box">Search</div>
    </div>
</div>
```
A narrow header with tabs:
```javascript
<div class="navbar mt-2" style="padding-top: 0; padding-bottom: 0;">
    <div class="navbar-narrow m-auto justify-content-between"
        style="margin-top: 0 !important; margin-bottom: 0 !important; height: 100%;">
        <a class="navbar-brand align-self-center"></a>
        <div class="nav-tabs d-flex flex-nowrap align-items-end">
            <a class="nav-item active" tabindex="0">Menu item 1</a>
            <a class="nav-item" tabindex="0">Menu item 2</a>
            <a class="nav-item" tabindex="0">Menu item 3</a>
            <a class="nav-item" tabindex="0">Menu item 4</a>
        </div>
    </div>
</div>
```
A small navbar with drop-down items, a secondary navbar with additional drop-down items and another secondary navbar with a breadcrumb: 
```javascript
<div class="navbar navbar-small mt-2 justify-content-start">
    <a class="navbar-brand"></a>
    <div class="nav-tabs d-flex flex-nowrap align-items-center">
        <a class="nav-item drop-down">Solutions</a>
        <a class="nav-item drop-down">Products</a>
        <a class="nav-item drop-down">Services</a>
        <a class="nav-item drop-down">Support & Downloads</a>
        <a class="nav-item drop-down">Partners</a>
    </div>
    <div class="search-container d-flex flex-nowrap ml-auto" style="width: 321px;">
        <div class="search-box">Search box</div>
        <div class="search-icon"></div>
    </div>
</div>
<div class="navbar-secondary d-flex align-items-center" style="height: 64px; padding-left: 16px;">
    <h1 style="margin-right: 136x; margin-bottom: 0">Healthcare</h1>
    <div class="nav-tabs d-flex flex-nowrap align-items-center">
        <a class="nav-item drop-down">Identity</a>
        <a class="nav-item drop-down">Mobility</a>
        <a class="nav-item drop-down">Intelligence</a>
    </div>
</div>
<div class="navbar-secondary d-flex align-items-center" style="height:40px; padding-left: 16px;">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb mt-2" style="background-color: white; padding: 5px 0 0 0">
            <li class="breadcrumb-item" style="padding-left: 0 !important;"><a href="#">Solutions</a></li>
            <li class="breadcrumb-item"><a href="#">Healthcare</a></li>
            <li class="breadcrumb-item active" aria-current="page">Patent Identity Management</li>
        </ol>
    </nav>
</div>
```