#Zebra Bootstrap Input Forms Documentation

###How to use? 

**Below are three versions of input forms, ready to copy and paste into your project.**
<br/>
*For a full reference please reference <https://getbootstrap.com/docs/4.0/components/forms/>*
<br/>
*For layout modification please reference <https://getbootstrap.com/docs/4.0/layout/grid/#auto-layout-columns>*

###Notes:
>Forms require zebra input.scss file for styling and color.scss for colors.
<br />
>Each from may require specific CSS, please see below forms 1 & 3.

##Input Forms (*3)

###1. Input Standard

<img src="standard_form.png" alt="Standard Input" width="350"/>

Form Specific CSS:

```javascript
<style>
    .zebraLabel {
      height: 28px;
      width: 192px;
      color: #000000;
      font-size: 18px;
      font-weight: 600;
      line-height: 28px;
      text-align: right;
      margin-bottom: 0px !important;
      margin-top: 8px;
    }

    .zebraLabelLong {
      height: 28px;
      width: 192px;
      color: #000000;
      font-size: 18px;
      font-weight: 600;
      line-height: 28px;
      text-align: right;
      margin-bottom: 0px !important;
    }

    .sbmt {
      margin-left: 10%;
    }
  </style>

```
HTML:

```javascript  
<div class="container-fluid">
    <!-- Standard Text Fields -->
    <form class="needs-validation" novalidate>
      <div class="row pt-2 pb-3">
        <label class="zebraLabel" for="name" class="col-sm-1 col-form-label"></label>
        <div class="col-sm-6">
          <input class="form-control" type="text" placeholder="Disabled" disabled>
        </div>
      </div>
      <div class="row pt-2 pb-3">
        <label class="zebraLabel" for="name" class="col-sm-1 col-form-label">Name*</label>
        <div class="col-sm-6">
          <input class="form-control zebraInput" type="text" class="form-control" id="inputName" placeholder="James">
          <div class="invalid-feedback">
            Please enter a Name.
          </div>
        </div>
      </div>
      <div class="row pt-2 pb-3">
        <label class="zebraLabel" for="name" class="col-sm-1 col-form-label">Company*</label>
        <div class="col-sm-6">
          <input class="form-control zebraInput" type="text" class="form-control" id="inputCompany" placeholder="Brown Industries" required>
          <div class="invalid-feedback">
            Please enter a Company.
          </div>
        </div>
      </div>
      <div class="sbmt">
        <div class="col-sm-6">
          <small id="mandatory" class="form-text text-muted">*: Mandatory fields</small>
        </div>

        <div class="col-sm-6">
          <button class="btn btn-primary mt-2" type="submit">Submit form</button>
        </div>

      </div>
    </form>
    <!-- Standard Text Fields -->
  </div>
```

***

###2. Input Above

<img src="InputAbove_form.png" alt="Input Above" width="300"/>

HTML

```javascript
<div class="container-fluid">
    <form class="form needs-validation" novalidate>
      
      <div class="form-label-group float-label-outer">
        <label for="inputName">*Name:</label>
        <input type="text" id="inputName" class="form-control" placeholder="James" required>
        <div class="invalid-feedback">
          Please enter a Name.
        </div>
      </div>

      <div class="form-label-group float-label-outer">
        <label for="inputCompany">*Company:</label>
        <input type="text" id="inputCompany" class="form-control" placeholder="Brown Industries" required>
        <div class="invalid-feedback">
          Please enter a Company.
        </div>
      </div>

      <small id="mandatory" class="form-text text-muted">*: Mandatory fields</small>

      <button class="btn btn-primary mt-2" type="submit">Submit form</button>

    </form>
  </div>
```

***

###3. Input Floating

<img src="InputFloating_form.png" alt="Input Floating" width="300"/>

Form Specific CSS:

```javascript
<style>
    input {
      padding-top: 25px !important;
    }
  </style>
```

HTML:

```javascript
  <div class="container-fluid mt-4">
    <form class="form needs-validation" novalidate>

      <div class="form-label-group float-label-inner">
        <input type="text" id="inputName" class="form-control" placeholder="" required>
        <label for="inputName">*Name:</label>
        <div class="invalid-feedback">
          Please enter a Name.
        </div>
      </div>

      <div class="form-label-group float-label-inner">
        <input type="text" id="inputCompany" class="form-control" placeholder="" required>
        <label for="inputCompany">*Company:</label>
        <div class="invalid-feedback">
          Please enter a Company.
        </div>
      </div>

      <small id="mandatory" class="form-text text-muted">*: Mandatory fields</small>


      <button class="btn btn-primary mt-2" type="submit">Submit form</button>

    </form>
  </div>
```

***


