###How to use?
For a full reference go to (https://getbootstrap.com/docs/4.0/components/navs/#tabs)

##Tab States
- Default: The default tab has a grey bottom-border and light-grey text.
- Selected: Used by applying the .active or .active-strong classes to a tab. 
- Disabled: Used by applying the .disabled class to a tab.

##Usage
- To achieve the correct styling, tabs must be in a '<nav>' container with the .navbar class applied.

##Example
Here's an example set of tabs with a tab of each type:
```javascript
<nav class="navbar nav-justified navbar-expand-sm navbar-light">
    <div class="navbar-nav">
        <a class="nav-item nav-link active" href="#">Tab 1</a>
        <a class="nav-item nav-link" href="#">Tab 2</a>
        <a class="nav-item nav-link active-strong" href="#">Tab 3</a>
        <a class="nav-item nav-link" href="#">Tab 4</a>
        <a class="nav-item nav-link disabled" href="#">Tab 5</a>
    </div>
</nav>
```