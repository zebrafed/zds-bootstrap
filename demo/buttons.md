#Zebra Bootstrap Buttons Documentation

###How to use? 
For a full reference please examine (https://getbootstrap.com/docs/4.0/components/buttons/)

##List of buttons overriden:  
- btn-primary: add the .btn-primary class to add our primary Zebra blue theme to a button. 
- btn-secondary: add the .btn-secondary class to add our secondary Zebra grey theme to a button. 
- btn-outline: add .btn-outline to create an outline themed button. 
- **Please note these docs expect the compulsary use of the .btn class for each button created.** 
##Button Sizes
- Default Size: add nothing, the .btn class has our default size attached. 
- Small Size: add the .btn-sm class to the button.

##Creating an Image Button
To ensure the correct alignment, when creating an image button please follow these steps:
- Add a div of class 'flexparent' which contains a div of class 'flexitem' for the text to display on the button, followed by and an <Img ..>. For example:
```javascript
<div class='flexparent'>
    <div class='flexitem'>
        Button Text
    </div>
     <Img src='...' ... />
<div>
```

