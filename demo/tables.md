#Zebra Bootstrap Tables Documentation

###How to use? 
For a full reference please examine (https://getbootstrap.com/docs/4.0/content/tables/)

##List of tables overriden:  
- Striped: Use .table-striped to add zebra-striping to any table row within the <tbody>.
- Basic: Add nothing.
- Dense: Add .table-sm to make tables more compact.
- Bordered: Add .table-bordered for borders on all sides of the table and cells.
- **Please note these docs expect the compulsary use of the .table class for each table created.** 

##List of features overriden:
- Hover: Add .table-hover to enable a hover state on table rows within a <tbody>.

#Pagination
- **Ul**: To access the pagination container, please attach the **pagination** class.
- **Li**: Button arrows (next and previous), please attach the li class **btn-arrow** and **back** or **next** as appropriate
- **Li** For each page item, attach the li class **page-item**

##Example use
For a bordered table with pagination one might call:
```javascript
<table class="table table-bordered table-hover">
    <thead>
        <th scope="col">Heading 1</th>
        <th scope="col">Heading 2</th>
    </thead>
    <tbody>
        <tr>
            <td>Content 1</td>
            <td>Content 1</td>
        </tr>
        <tr>
            <td>Content 2</td>
            <td>Content 2</td>
        </tr>
        <tr>
            <td>Content 3</td>
            <td>Content 3</td>
        </tr>
    </tbody>
</table>
<ul class="pagination">
    <li class="btn-arrow back"></li>
    <li class="page-item selected">1</li>
    <li class="page-item">2</li>
    <li class="btn-arrow next"></li>
</ul>
```


