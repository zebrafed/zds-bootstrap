##How to use
For a full reference, go to (https://getbootstrap.com/docs/4.0/components/modal/)

###Making a modal
- Apply the .modal class to the parent container for the modal.
- Headers should be put in a sub-container with the .modal-header class applied.
- The body of the modal should be in a seperate sub-container with the .modal-body class applied.
- Any content to go below the body should be put in another sub-container with the .modal-footer class applied.

###Example
Here's an example modal with an acconpanying button to open it:
```javascript
<button class="btn btn-primary mt-2" data-toggle="modal" data-target="#example-dialog-box">Open dialog box</button>
<div class="modal" tabindex="-1" id="example-dialog-box">
<div class="modal-header">
    <button class="close" data-dismiss="modal"></button>
</div>
<div class="modal-body">
    <h1 class="card-subtitle">Dialog boxes</h1>
    <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
    incididunt ut labore et dolore magna aliqua.
    Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
</div>
<div class="modal-footer flex-margin-2">
    <button class="btn btn-secondary" style="margin-right: 24px;">Cancel</button>
    <button class="btn btn-primary">Save changes</button>
</div>
</div>
```