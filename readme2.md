Bootstrap overrides:
btn
btn-primary
btn-secondary
btn-outline
btn-sm

Also, create image buttons using the following syntax:
<button class="btn btn-primary btn-sm">
                    <div class='flexparent'>
                            <div class='flexitem'>
                                Txt
                            </div>
                                Icon
                        </div>
</button>