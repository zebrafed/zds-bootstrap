# Bootstrap Theme Kit: Make your own Bootstrap Themes

This is simple starter project to help you get started quickly when making a custom Bootstrap theme.
The Zebra team has implemented the Zebra Design System into this project.

## Prerequisites

- This works on Windows, macOS and Linux.
- Yarn, Node Package Manager, and Gulp are required. Make sure you can run `yarn -v`, `gulp -v`, and `npm -v`.
- You can get Node at [nodejs.org](https://nodejs.org), then install gulp using `npm install gulp-cli -g`

## Getting started

1. Run `yarn`
2. Run `gulp watch`
3. Navigate to the Local Server described when running `gulp watch` (normally <http://localhost:3000>)
4. Add any Bootstrap Sass variables into an scss file in the scss directory.
5. If you have created new scss files, add them to `scss/zebra-theme.scss` import section


## Build Production Code

1. Run `gulp`
2. Production files `zebra-theme.css` and `zebra-theme.css` will be output into `/css` folder. These css files include the bootstrap version defined in package.json