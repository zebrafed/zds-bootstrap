var gulp  = require('gulp'),
  sass = require('gulp-sass'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  cleanCss = require('gulp-clean-css'),
  rename = require('gulp-rename'),
  postcss      = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  browserSync = require('browser-sync').create();

function buildCss() {
    return gulp.src(['scss/zebra-theme.scss'])
        .pipe(sourcemaps.init())
        //.pipe(concat('zebra-theme.scss'))
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer()]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css/'))
        .pipe(cleanCss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css/'))
}

function watcher() {
    gulp.watch(['scss/*.scss'], gulp.series(buildCss));
}
// Static server
function browzSync() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
}

exports.watch = gulp.series(buildCss, gulp.parallel(watcher, browzSync));
exports.default = gulp.series(buildCss);
